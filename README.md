# Track processor
This is the repository for an API service that processes incoming track definitions and sends them to get processed by different departments based on their given tags

### How to run locally 
To run this locally on one's machine, one must have 
- node (18 recomended)
- yarn or npm

Once the repository has been cloned on your machine, run:
```
yarn install OR npm install
yarn start OR npm run start
```


#### Seeding data

Before running the code, it is recomended to seed the database with some departments and tags, run the following:
```
yarn seed
```


### API usage
The API is accessible on localhost:8081. It is recomended to use an API client like Postman or Insomnia or curl requests via the terminal. See the endpoints implemented and their expected request objects:

#### Sending a track to process

POST /tracks
```
{
    "name": "zoom zoom"
    "tags": ["acid", "techno"],
    "advance": 1800
}
```
Response:
```
{
	"status": "success",
	"data": {
		"name": "zoom zoom",
		"tags": [
			"acid",
			"techno",
			"club"
		],
		"processedInfo": {
			"Acid": "waiting for gen. management",
			"EDM": "being processed by EDM department"
		}
	}
}
```
#### Adding a department

POST /departments
```
{
	"name": "Soul",
	"tags": ["r&b", "blues"],
	"threshold": 1500
}
```
Response:
```
{
	"status": "success",
	"data": {
		"createdDepartment": {
			"name": "Soul",
			"threshold": 1500,
			"id": 8,
			"tags": [
				"r&b",
				"blues"
			]
		}
	}
}
```

#### Getting all departments

GET /departments
Response:
```
[
	{
		"department": {
			"id": 1,
			"name": "Trance",
			"threshold": 3000,
			"tags": [
				{
					"id": 2,
					"name": "trance",
					"departmentId": 1
				}
			]
		},
	},
	{
		"department": {
			"id": 2,
			"name": "EDM",
            ....
]   

```
### File structure

The codebase is split into the following folders:
- api : contains the routes/endpoints used and the validator
- database: contains the entities file and the sqlite database (and its seeder)
- services: contains the service methods called by the endpoints
- tests: contains the tests run by jest

### Testing
There are unit tests present in the tests directory, run them via the following command:

```
yarn test
```

#### Data Structure
The following entities are present in this service:
- **Department** - Entities that are responsible for processing and approving tracks
- **Track** - a song received by us that needs to be processed by departments
- **Tag** - a way to classify track definitions based off how they sound.
- **ProcessedTrack** - when a department processes a track, a processed track is created with its own status







![alt text](https://bengijzel.ams3.cdn.digitaloceanspaces.com/track_processor.png "Title Text")

