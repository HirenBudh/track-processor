import express from "express"
import router from "./api/routes"
import "reflect-metadata"

const app = express()
const port = 8081
app.use(express.json())
app.use(router)

app.listen(port, () => {
  console.log(`Server running on http://localhost:${port}`)
})