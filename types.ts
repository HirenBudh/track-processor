export interface IDepartment {
    id?: number
    name: string
    threshold?: number
    tags?: string[]
}

export interface ITag {
    id?: number
    name: string
    departmentId: number
}

export interface ITrack {
    id?: number
    name: string
    advance?: number
    tags?: string[]
}