import { ITrack } from "../types"

export function validateDepartment(data: any): { valid: boolean, missingProps?: string[], invalidProps?: string[] } {
    let valid = true
    const missingProps: string[] = []
    const invalidProps: string[] = []
  
    if (!data) {
        return { valid: false, missingProps: ["data is null or undefined"] }
    }
  
    // Check 'name' property
    if (typeof data.name !== "string") {
        valid = false
        if (data.name === undefined) {
            missingProps.push("name")
        } else {
            invalidProps.push("name")
        }
    }
  
    // Check 'threshold' property
    if (typeof data.threshold !== "number") {
        valid = false
        if (data.threshold === undefined) {
            missingProps.push("threshold")
        } else {
            invalidProps.push("threshold")
        }
    }

     // Check 'tags' property
    if(data.tags) {
        if (!Array.isArray(data.tags) || !data.tags.every((tag: any) => typeof tag === "string") || data.tags.length < 1) {
            valid = false
            invalidProps.push("tags")
        }
    }
  
    return {
        valid,
        missingProps: missingProps.length > 0 ? missingProps : undefined,
        invalidProps: invalidProps.length > 0 ? invalidProps : undefined
    }
}

export function validateTrack(data: ITrack): { valid: boolean, missingProps?: string[], invalidProps?: string[] } {
    let valid = true
    const missingProps: string[] = []
    const invalidProps: string[] = []
  
    if (!data) {
        return { valid: false, missingProps: ["data is null or undefined"] }
    }
  
    // Check 'name' property
    if (typeof data.name !== "string" || data.name === null) {
        valid = false
        if (data.name === undefined) {
            missingProps.push("name")
        } else {
            invalidProps.push("name")
        }
    }

    // Check 'tags' property
    if (!Array.isArray(data.tags) || !data.tags.every(tag => typeof tag === "string") || data.tags.length < 1) {
        valid = false
        if (data.tags === undefined) {
            missingProps.push("tags")
        } else {
            invalidProps.push("tags")
        }
    }

    // Check 'advance' property if it exists
    if (data.advance !== undefined && (typeof data.advance !== "number" || !Number.isInteger(data.advance))) {
        valid = false
        invalidProps.push("advance")
    }
  
    return {
        valid,
        missingProps: missingProps.length > 0 ? missingProps : undefined,
        invalidProps: invalidProps.length > 0 ? invalidProps : undefined
    }
}