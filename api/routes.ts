import express from "express"
import { DepartmentService } from "../services/department-service"
import { IDepartment, ITrack } from "../types"
import { initializeDataSource } from "../ormconfig"
import { DataSource } from "typeorm"
import { validateDepartment, validateTrack } from "./validator"
import { TrackProcessingService } from "../services/track-processing-service"

const router = express.Router()
let dataSource: DataSource

const initDataSource = async () => {
	dataSource = await initializeDataSource()
}

initDataSource().catch(err => console.error("Failed to initialize data source:", err))

const handleErrors = (error: any, res: express.Response) => {
	console.error(error)
	res.status(500).json({
		status: "error",
		message: "An error occurred while processing."
	})
}

router.post("/tracks", async (req, res) => {
	const trackValidation = validateTrack(req.body)
	if (!trackValidation.valid) {
		return res.status(400).json(trackValidation)
	}

	try {
		const trackData: ITrack = req.body
		const trackProcessingService = new TrackProcessingService(dataSource)
		const departmentFeedback = await trackProcessingService.processTrack(trackData)
		res.status(201).json({
			status: "success",
			data: {
				name: trackData.name,
				tags: trackData.tags,
				departmentFeedback
			}
		})
	} catch (error) {
		handleErrors(error, res)
	}
})

router.get("/departments", async (req, res) => {
	try {
		const departmentService = new DepartmentService(dataSource)
		const departments = await departmentService.getAll()
		res.json(departments)
	} catch (error) {
		handleErrors(error, res)
	}
})

router.post("/departments", async (req, res) => {
	const departmentValidation = validateDepartment(req.body)
	if (!departmentValidation.valid) {
		return res.status(400).json(departmentValidation)
	}

	try {
		const departmentData: IDepartment = req.body
		const departmentService = new DepartmentService(dataSource)
		const createdDepartment = await departmentService.createDepartment(departmentData)
		res.status(201).json({
			status: "success",
			data: {
				createdDepartment
			}
		})
	} catch (error) {
		handleErrors(error, res)
	}
})

router.get("/test", (req, res) => {
	res.setHeader("Content-Type", "text/plain")
	res.send(`test train choo choo

  |DD|____T_
  |_ |_____|<
    @-@-@-oo\\
  `)
})

export default router
