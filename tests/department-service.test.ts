import { DataSource } from "typeorm"
import { DepartmentService } from "../services/department-service"
import { IDepartment } from "../types"
import { initializeDataSource } from "../tests/test-ormconfig"
import { seedTestDatabase } from "./test-seeder"
import { Department, ProcessedTrack, Tag, Track } from "../database/entities"

describe("DepartmentService", () => {
    let service: DepartmentService
    let dataSource: DataSource

    beforeAll(async () => {
        dataSource = await initializeDataSource()
        service = new DepartmentService(dataSource)
        await seedTestDatabase()
            .then(() => {
                console.log("Test Seeding complete!")
            })
            .catch((error) => {
                console.error("Test Seeding failed!", error)
            })
    })

    afterAll(async () => {
        await dataSource.manager.clear(Tag)
        await dataSource.manager.clear(ProcessedTrack)
        await dataSource.manager.clear(Track)
        await dataSource.manager.clear(Department)
    })

    it("should create a department correctly", async () => {
        const departmentData: IDepartment = {
            name: "Dance",
            threshold: 3000
        }

        const department = await service.createDepartment(departmentData)

        expect(department).toBeDefined()
        expect(department.id).toBeDefined()
    })

    it("should create a department with tags correctly", async () => {
        const departmentData: IDepartment = {
            name: "Urban",
            threshold: 3000,
            tags: ["hiphop", "r&b", "drill"]
        }

        const department = await service.createDepartment(departmentData)
        expect(department).toBeDefined()
        expect(department.id).toBeDefined()
        expect(department.tags).toEqual(["hiphop", "r&b", "drill"])
    })

    it("should fetch all departments with tags correctly", async () => {
        const allDepartments = await service.getAll()

        expect(allDepartments).toBeDefined()
        expect(allDepartments.length).toBeGreaterThan(0)

        const someDepartment = allDepartments[0]
        expect(someDepartment.department).toBeDefined()
        expect(someDepartment.tags).toBeDefined()

        // Validate the tags
        expect(someDepartment.tags).toBeDefined()
    })

    // Unhappy path tests
    it("should handle error when creating a department with invalid data", async () => {
        const departmentData: any = {
            names: "Bad attribute name",
            threshold: 3000
        }

        await expect(service.createDepartment(departmentData)).rejects.toThrow()
    })
})