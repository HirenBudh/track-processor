import { DataSource } from "typeorm"
import { TrackProcessingService } from "../services/track-processing-service"
import { ITrack } from "../types"
import { initializeDataSource } from "../tests/test-ormconfig"
import { Department, ProcessedTrack, Tag, Track } from "../database/entities"
import { seedTestDatabase } from "./test-seeder"

describe("TrackProcessingService", () => {
    let service: TrackProcessingService
    let dataSource: DataSource

    beforeAll(async () => {
        // Connect to your test database here
        dataSource = await initializeDataSource() // Or however you're supposed to get this with your setup
        service = new TrackProcessingService(dataSource)
        await seedTestDatabase()
            .then(() => {
                console.log("Test Seeding complete!")
            })
            .catch((error) => {
                console.error("Test Seeding failed!", error)
            })
    })

    afterAll(async () => {
        await dataSource.manager.clear(Tag)
        await dataSource.manager.clear(ProcessedTrack)
        await dataSource.manager.clear(Track)
        await dataSource.manager.clear(Department)
    })

    it("should process a track correctly", async () => {
        const trackData: ITrack = {
            "name": "test the best",
            "tags": ["techno"],
            "advance": 2000
        }

        const result = await service.processTrack(trackData)
        expect(result).toBeDefined()
        // Assert that EDM department is in the result and has the correct status
        expect(result).toHaveProperty("EDM")
        expect(result["EDM"]).toBe("being processed by EDM department")

        // Check if ProcessedTrack has an entry for this track with the EDM department
        const edmDepartment = await dataSource.manager.findOne(Department, { where: { name: "EDM" } })
        expect(edmDepartment).toBeDefined()

        const processedTracks = await dataSource.manager.find(ProcessedTrack, { where: { department: edmDepartment || undefined } })
        expect(processedTracks).toBeDefined()
        expect(processedTracks.length).toBeGreaterThan(0)

        const processedTrack = processedTracks.find(pt => pt.track.name === "test the best")
        expect(processedTrack).toBeDefined()
        expect(processedTrack!.status).toBe("being processed by EDM department")
    })
})