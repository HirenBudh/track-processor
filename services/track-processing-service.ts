import { DataSource } from "typeorm"
import { ITrack } from "../types"
import { Department, ProcessedTrack, Tag, Track } from "../database/entities"

export class TrackProcessingService {
  private dataSource: DataSource
  constructor(dataSource: DataSource) {
    this.dataSource = dataSource
  }

  async processTrack(trackData: ITrack) {
    // Create a new Track entity
    const newTrack = await this.dataSource.manager.save(Track, {
      ...trackData,
      tags: Array.isArray(trackData.tags) ? trackData.tags.join(",") : trackData.tags,
    })

    // Holds information on processed departments and their statuses for response
    const processedInfo: { [key: string]: string } = {}

    // Iterate through track tags to get processing requirements
    const procesingDepartments: Department[] = []
    const defaultDepartment = await this.dataSource.manager.findOne(Department, { where: { name: "EDM" } })

    for (const tagName of trackData.tags || []) {
      const tag = await this.dataSource.manager.findOne(Tag, { where: { name: tagName } })
      if (tag !== null) {
        const department = await this.dataSource.manager.findOne(Department, { where: { id: tag.departmentId } })
        if (department !== null) procesingDepartments.push(department)
      } else {
        procesingDepartments.push(defaultDepartment!)
      }
    }

    const uniqueProcessingDepartments = procesingDepartments.reduce((unique: Department[], department: Department) => {
      if (!unique.some(dep => dep.id === department.id)) {
        unique.push(department)
      }
      return unique
    }, [])

    for (const department of uniqueProcessingDepartments) {
      let status = ""
      if (!newTrack.advance || newTrack.advance < department.threshold) {
        status = `being processed by ${department.name} department`
        this.dataSource.manager.create(ProcessedTrack, {
          department,
          track: newTrack,
          status
        })
      } else {
        status = "waiting for gen. management"
        this.dataSource.manager.create(ProcessedTrack, {
          department,
          track: newTrack,
          status
        })
      }

      await this.dataSource.manager.save(ProcessedTrack, {
        track: newTrack,
        department,
        status
      })
      processedInfo[department.name] = status
    }

    return processedInfo
  }
}
