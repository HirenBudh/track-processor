import { DataSource } from "typeorm"
import { Department, Tag } from "../database/entities"
import { IDepartment } from "../types"

export class DepartmentService {
    private dataSource: DataSource

    constructor(dataSource: DataSource) {
        this.dataSource = dataSource
    }

    async createDepartment(department: IDepartment) {
        let savedDepartment: any
        try {
            // First, save the department data
            savedDepartment = await this.dataSource.manager.save(Department, {
                name: department.name,
                threshold: department.threshold
            })
        } catch (error) {
            console.error("Error while saving department:", error)
            throw new Error("Failed to save department")
        }
    
        // If tags are provided, save them
        if (department.tags && savedDepartment.id) {
            for (const tagName of department.tags) {
                try {
                    await this.dataSource.manager.save(Tag, {
                        name: tagName,
                        departmentId: savedDepartment.id
                    })
                } catch (error) {
                    console.error("Error while saving tag:", error)
                    throw new Error("Failed to save tag")
                }
            }
        }
    
        return department.tags ? { ...savedDepartment, tags: department.tags } : savedDepartment
    }
    
    async getAll(): Promise<Array<{ department: Department, tags: Tag[] }>> {
        const departments = await this.dataSource.manager.find(Department)
        
        return Promise.all(
            departments.map(async department => {
                const tags = await this.dataSource.manager.find(Tag, {
                    where: {
                        department
                    }
                })

                return { department, tags }
            })
        )
    }
}