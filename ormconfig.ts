import { DataSource } from "typeorm"
import { Department, ProcessedTrack, Tag, Track } from "./database/entities"

export async function initializeDataSource() {
  const AppDataSource = new DataSource({
    type: "sqlite",
    database: "./database/database.sqlite",
    entities: [
      Department,
      Tag,
      ProcessedTrack,
      Track
    ],
    synchronize: true,
  })

  return AppDataSource.initialize()
    .then(() => {
      console.log("Data Source has been initialized!")
      return AppDataSource
    })
    .catch((err) => {
      console.error("Error during Data Source initialization", err)
      throw err
    })
}