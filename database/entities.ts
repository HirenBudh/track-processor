import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToMany,
} from "typeorm"

@Entity()
export class Department {
  @PrimaryGeneratedColumn()
  id: number

  @Column()
  name: string

  @Column("int")
  threshold: number
  
  @OneToMany(() => Tag, (tag) => tag.department, {eager: true})
  tags: Tag[]

  @OneToMany(() => ProcessedTrack, (processedTrack) => processedTrack.department)
  processedTracks: ProcessedTrack[]
}

@Entity()
export class Tag {
  @PrimaryGeneratedColumn()
  id: number

  @Column()
  name: string

  @Column()
  departmentId: number

  @ManyToOne(() => Department, (department) => department.tags)
  department: Department
}

@Entity()
export class Track {
  @PrimaryGeneratedColumn()
  id: number

  @Column()
  name: string

  @Column("int", { nullable: true })
  advance?: number

  @Column({ type: 'text', nullable: true })
  tags?: string  // serialized list of tags

  // OneToMany relationship with ProcessedTrack
  @OneToMany(() => ProcessedTrack, (processedTrack) => processedTrack.track)
  processedTracks: ProcessedTrack[]
}

@Entity()
export class ProcessedTrack {
  @PrimaryGeneratedColumn()
  id: number

  @Column()
  status: string

  @ManyToOne(() => Track, (track) => track.processedTracks, { eager: true })
  track: Track

  @ManyToOne(() => Department, (department) => department.processedTracks)
  department: Department
}
