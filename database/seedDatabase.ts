import { Department, ProcessedTrack, Tag, Track } from "./entities"
import { initializeDataSource } from "../ormconfig"

async function seedDatabase() {

    const AppDataSource = await initializeDataSource()
   
    // Clear existing data
    await AppDataSource.manager.clear(Tag)
    await AppDataSource.manager.clear(ProcessedTrack)
    await AppDataSource.manager.clear(Track)
    await AppDataSource.manager.clear(Department)

    // Seed Departments
    const departments = [
        { id: 1, name: "Trance", threshold: 3000 },
        { id: 2, name: "EDM", threshold: 4000 },
        { id: 3, name: "Acid", threshold: 1500 },
    ]
    const savedDepartments = await AppDataSource.manager.save(Department, departments)

    console.log("savedDepts", savedDepartments)

    // Seed Tags
    const tags = [
        { name: "acid", department: savedDepartments.find((d) => d.id === 3) },
        { name: "trance", department: savedDepartments.find((d) => d.id === 1) },
        { name: "techno", department: savedDepartments.find((d) => d.id === 2) },
        { name: "club", department: savedDepartments.find((d) => d.id === 2) },
        { name: "deep", department: savedDepartments.find((d) => d.id === 2) },
    ]

    let tagId = 1
    const savedTags = await AppDataSource.manager.save(Tag, tags.map((tag) => {
        const tagEntity = new Tag()
        tagEntity.id = tagId
        tagEntity.name = tag.name
        tagEntity.departmentId = tag.department?.id || 2
        tagId++
        return tagEntity
    }))

    console.log("savedTags", savedTags)
}

seedDatabase()
    .then(() => {
        console.log("Seeding complete!")
    })
    .catch((error) => {
        console.error("Seeding failed!", error)
    })
